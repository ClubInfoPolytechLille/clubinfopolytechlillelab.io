---
title: "2021-2022"
---

- **Président :** Axel Chemin
- **Vice-président :** Titouan Azimzadeh
- **Trésorier :** Clément Godet
- **Secrétaire :** Nicolas Gutierrez
- **Respo Com :** Hugo Aillerie
- **Respo event :** Théo Lanord
