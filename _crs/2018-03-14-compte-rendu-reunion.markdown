---
date: 2018-03-14 13:45:00 +0100
author: "Blas Simon"
tags: ci
---

Bonjour à tous! Voici le compte rendu du 14 mars 2018.

Au programme aujourd'hui:
- Tuto Ruby
- Vidéo de présentation
- Chasse au trésor
- Battle dev
- Nuit de l'info

### Chasse au trésor

Quelques idées on déjà été proposées, mais il nous en faut encore, n'hésitez pas à en proposer.
Une conversation sur le discord du club info a été créée pour pouvoir en discuter.

### Nuit de l'info

Les préparations commenceront bientôt.

### Battle dev

Il y aura des préparations à la prochaine réunion, ainsi que  le week-end précédant la battle dev,
soit les samedi 24 et dimanche 25. La battle dev sera mardi 27 au soir, à 20h00. 
Rendez vous à tous sur la conversation discord dédiée!

Vous n'êtes pas obligés de vous inscrire pour y participer :), et nous essaierons de tous le réaliser en Python.

### Vidéo de présentation

Elle sera faite en scratch.
Dedans il y aura:
- la toucharme du club info

#### Fin de la réunion