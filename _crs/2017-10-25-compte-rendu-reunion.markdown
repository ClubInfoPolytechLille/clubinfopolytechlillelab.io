---
date:   2017-10-25 13:45:00 +0100
author: "Simon Blas"
tags: ci
---


## Bonjour à toutez!

### Ordre du jour

- Infos sur le site web qui avance + questions
- Le fil rouge nuit de l'info
- Une proposition de jean
- Une présentation de Git Kraken
- Bonnes vacances!!

A venir:

- Des tutos pour la nuit de l'info

#### Pour commencer:

On a un nouveau!! Encore!! Il s'appelle Alex, aka N7. Soit le bienvenu!

## Jean

Jean a 23 ans.
Il nous a parlé du défi H.
C'est un défi entre les écoles d'ingé et d'informatique, organisé par Sogeti, grande entreprise dans l'informatique.
Il y aura des équipes de 4 à 6 membres, pour travailler sur un projet. 
L'année dernière, l'équipe à utilisé un casque de VR pour sourds et malentendants, en traduisant les choses dites par leur interlocuteur en language des signes.
Jean veut donc motiver des gens cette année pour constituer une équipe de 6.
Ses idées sont:

- traduction language des signes to talk

- traduction braille to talk

- exosquelette

- manipulation cabotique?

Les inscriptions se finiront le 16 Novembre, et le concours se termine en Mai 2018.
On garde la mainmise sur nos idées, Sogeti ne possèdera pas nos trouvailles et on peut se faire remarquer.
On sera encadrés par quelqu'un de Sogeti, une association partenaire trouvée par Jean et un membre de Polytech.
Ca peut déboucher sur un stage et ça serait valorisant sur un CV. 
Le partenariat avec Sogeti enlève les restrictions budgetaires, le casque utilisé l'année dernière coûtait 6000 euros. 

Si vous voulez le contacter, son nom est Jean Vitali.

## La nuit de l'info

Postez vos idées de noms d'équipe dans le post correspondant!
On aura une équipe de plus que 10, on compte sur Benoît pour que ça passe.
On a trois domaines différents, choisissez votre camp:

- Gérer tout ce beau monde: Simon Maëva 

- FrontEnd (Css, Html, Php): Benoit Nicolas

- BackEnd (Serveur): Alex Clément Baptiste 

- Base de données (Sql): Justine Hugo Thomas

- Vidéo: Eloi Geoffrey


Si vous n'êtes pas dans cette liste et voulez participer à la nuit de l'info, postez vous en commentaire avec votre camp!

## Benoit nous montre Git Kraken

Git c'est cool.

## Benoit nous montre le site du Bde

Si vous voulez proposer votre aide n'hésitez pas! Ils ne sont que deux sur un site, c'est un peu light.
Notre grand Hugo se propose! L'équipe sera donc constituée de trois personnes!


Fin de la réunion! 

# De très bonnes vacances à tous! Et à toutes! Et à toutzs! :D