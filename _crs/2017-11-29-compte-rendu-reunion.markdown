---
date:   2017-11-29 13:45:00 +0100
author: "Simon Blas"
tags: ci
---

##### Bonjour à toutes et à tous!

Voici le compte rendu de la réunion du 29 novembre 2017.

## Nuit de l'info T-2 semaines!

Ordre du jour:

- Partenariat CI - Eestec?
- Rappel photo
- Rappel install party
- Point vêtement?
- Johnny recherche développeur
- Tuto nuit de l'info

## Partenariat Club Info - Eestec:

L'eestec au niveau européen cherche des membres pour faire du développement web. 
Mais manifestement, personne n'est intéressé, nous ne répondrons donc pas à cette
offre.

## La photo

La photo, c'est ce vendredi, à 10h00! Rendez vous dans le hall. Venez en gris si possible.
C'est gratuit d'être pris en photo, si vous voulez ensuite l'acheter, il y a un 
post Facebook sur la page du club Info dans ce but.

## Install Party

Rendez-vous pour tout ceux qui participent à la nuit de l'info chez Benoit à 15h00, ce samedi! (le samedi 2 décembre).
C'est important, tout le monde doit avoir les mêmes logiciels prêts pour la NDL.

#### L'adresse est 40 rue pasteur, 59 260, Lezennes

Le moyen le plus simple d'y aller est le métro+bus, avec le bus numéro 18. 
Depuis le métro, vous pouvez également y aller à pied, pour un temps de marche de 15 mins, ou de course de 5 mins.

## On n'oublie pas le point vêtement

Il y a un post Facebook si quelqu'un voudrait un polo ou un sweat avec le logo
du club Info. Il y a un post facebook à cet effet, et si personne n'est intéressé,
ça arrangera notre tréz, moins de complications.

## Recherche développeur Android

Johnny aurait besoin d'un développeur Android, pour un projet de deux portables qui communiquent entre eux.
C'est un gros projet, qui demande des compétences solides. Si vous êtes intéressés, contactez Johnny Gouvaert.

## Tuto Html, Ajax, Css

On a le budget pour faire des diapos sans projecteur! :)
A destination de Nicolas, Benoit et Robin! Un tuto Front End.


#### Voila voila, fin de la réunion, une bonne journée à tous et à toutes! :D


