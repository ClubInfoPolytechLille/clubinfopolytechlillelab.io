---
author: "Blas Simon"
tags: ci
date: 2018-03-28 13:45:00+02:00
---

Bonjour à tous, voici le compte rendu du 28 mars 2018!

## Passations

Il y aura ce soir l'Afterwork passations, et quelques membres y seront, mais représentant d'autres clubs.


## Battle Dev

Le discord était une excellente idée, ça s'est très bien passé. 
Les retours des gens:
- Justine a aimé, c'était sympa de travailler ensembles

## Sites pour les clubs

On pensait proposer aux clubs de leur faire un site, et le leur mettre sur Gitlab,
en échange d'un petit finacement.

Le site du Bde continue à avancer lentement, nous allons le proposer au nouveau Bde prochainement.

Une idée est de faire un site vitrine gratuitement pour l'esteec, ce qui nous fera 
de la pub pour encourager d'autres clubs à accepter notre proposition. L'entente Esteec-Club Info est importante.
On va soit mettre à jour leur site déjà existant, soit leur en faire un nouveau, en fonction de ce qui est le plus simple.

## Vidéo de début d'année

On a déjà un début de script de vidéo, il sera réalisé en collaboration sur notre Discord, sur un chaine dédiée.

## La chasse au trésor

Notre ex(mais pas vraiment)-président nous propose de faire un résumé de l'histoire autour du jeu de piste.
Il a déjà commencé à réaliser des choses. Il faudra faire un descriptif des défis, et trouver des images en rapport.
Tout est sur Discord, allez voir! :) On va se répartir les différents défis afin de rédiger des descriptifs de chacun.
On a un total de 11 défis pour le moment.

Pour info, notre serveur n'est plus hébergé par Geoffrey. Nous sommes indépendants.
La proposition Tinder'go pour le club Info semble êtres une bonne idée, notamment
pour l'intégration.

Les récompenses seront des Autocollants, des jetons, des éco-cups, des dessous de verre et des goodies réalisés au Fablab.

On prévoit également de modifier le logo du club Info.
Voila voila, fin de la réunion!

Une bonne journée à tous!
 