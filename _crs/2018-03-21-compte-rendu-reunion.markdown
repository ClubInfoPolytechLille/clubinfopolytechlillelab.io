---
date: 2018-03-21 13:45:00 +0100
author: "Blas Simon"
tags: ci
---

Bonjour à tous! 

Johnny est président!

## La chasse au trésor

On a toujours besoin d'idées pour ça, publiez les sur le discord du club info ;).

## La vidéo de présentation du Club Info

Aujourd'hui, on prépare le script en scratch de la vidéo de présentation :).
- Nuit de l'Info
- Retranscrire une réunion du club Info, mais en scratch. Faire les voix.

Un tuto scratch en scratch sera fait à la première réunion du Club Info après les
vidéos de présentation.

## Les projets futurs

- Créer un tinder du club Info
- Contacter le nouveau Bde pour éventuellement finir le site du Bde
- Faire un tuto eject sur l'ordi de monsieur le directeur (avec son accord)

## Conclusion

Les feutres sont tout pourris. Johnny aime l'argent.

Fin de la réunion! Une bonne journée à tous :).