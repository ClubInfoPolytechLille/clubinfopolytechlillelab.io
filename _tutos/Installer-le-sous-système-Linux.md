---
title: "Windows 10 : Installer le sous-système Linux"
tags: WSL
author: "UnIma"
date: 2020-07-07 17:17:17
---

# Windows 10 : Installer le sous-système Linux

## Installer le terminal Linux dans Windows 10 afin d’utiliser les commandes git, make, gcc (et plein d’autres)  

Avec un sous-système Linux, on est en mesure d’utiliser tous les
programmes en ligne de commande de Linux directement depuis notre
session Windows. On ne pourra pas lancer de programme en mode interface
graphique.   

**Toutes les commandes et installations sont à faire sur
Windows 10.**

Paramètres &gt; Mise à jour et sécurité &gt; Espace développeurs :
cocher “Mode développeur”.

Paramètres &gt; Application &gt; Programmes et fonctionnalités &gt;
Paramètres associés (en haut à droite) : cliquer sur “Programmes et
fonctionnalités”.

Dans la colonne de gauche, cliquer sur “Activer ou désactiver des
fonctionnalités Windows”. Descendre la liste, et cocher l’option
“Sous-système Windows pour Linux” puis cliquer sur le “bouton OK”.

Redémarrer le PC.

Ensuite, se rendre sur le lien et télécharger le bash ubuntu :  
https://www.microsoft.com/fr-fr/p/ubuntu-1804-lts/9n9tngvndl3q  
Une fois téléchargé, cliquer sur “lancer”, après l’installation, taper son
nom d’utilisateur et son mot de passe.

Redémarrer le PC.

L’installation est terminée ! Maintenant, quand on veut ouvrir un
terminal Linux dans Windows, on ouvre l’explorateur de fichier Windows
puis on se rend dans le répertoire de notre choix. Ensuite, on double
clic sur la barre d’adresse des fichiers et on tape “bash” puis on
appuie sur “entrée”.

On peut aussi se rendre dans : Menu démarrer &gt; Toutes les
applications et cliquer sur « Ubuntu 18.04 LTS ».

*CC BY-NC-SA 3.0, 13-06-2020*  
https://creativecommons.org/licenses/by-nc-sa/3.0/fr/
