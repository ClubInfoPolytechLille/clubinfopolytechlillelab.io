---
title: "hardware"
tags: hardware
author: "Hugo Delbroucq"
date: 2017-05-24 13:34:17
---


Tuto hardware n°1 ( # VP pas si inutile) : Architecture PC

architecture  pc (source : marcel.developpez.com) :
vulgarisation-informatique.com

1. Le boitier et comment bien le choisir.

C'est lui qui contient  tous les composants de notre PC et qui a pour but de les
maintenir et les protéger (et de pas se brûler quand on veut déplacer notre
ordinateur). Il peut être sous le format d'une tour dans le cas d'un PC fixe où
toute la partie visible dans le cas du PC portable. Dans le cas de la tour, il
existe 3 types de format différents qui respectent la norme ATX :
Petit : ils ne disposent que de quelques emplacements pour les composants de
taille réduite et ne sont généralement utilisés qu'à des fins spécifiques telles
que la bureautique (traitement de texte etc) ou internet et visionnage de vidéos.
Ils fonctionnent très bien avec des cartes mères micro-ATX qui permettent des
configurations à encombrement réduit. On peut donc apparenter sa capacité de
calcul à celle d'une tablette portative
Moyen :  C'est le format de tour le plus utilisé à l'heure actuelle. Celui-ci
est plus volumineux mais permet de stocker un peu plus de composants et
fonctionne plutôt avec des cartes mères ATX standards. Ce sont des tours dont le
prix est plus faible que celui des grandes tours car bien moins puissantes mais
qui permettent un champs d'action plus important que les petites tours mais ne
comptez pas sur elles pour faire tourner skyrim en HD.
Grand : Il permet des configurations comprenant plusieurs cartes graphiques et
des disques durs en RAID( permet de créer une unité de stockage comportant
plusieurs disques durs et qui continue de fonctionner en cas de panne d'un
disque dur (donc c'est cool!) et permet d'augmenter la vitesse d'écriture (on
peut écrire sur plusieurs disques à la fois) mais sont plus coûteux.). C'est le
format qui permet le plus de folies au niveau de la configuration car l'espace
est suffisant pour stocker les composants et gérer la température qui peut
croître très vite.


2. La carte mère

La carte mère est le cerveau de l'ordinateur.	C'est grâce à elle que toutes
les connexions sont faites et elle est composé de plusieurs éléments.
Ces éléments sont les suivants :
Le chipset : Il a pour but de coordonner les échanges entre le processeur et les
autres composants. Il intègre à l'achat une puce graphique, réseau, audio etc
mais de faible performance.
Le SATA : un contrôleur qui permet d'augmenter la vitesse de transport de données
Le RAID : j'en ai parlé précédemment, c'est lui qui permet d'avoir une unité de
stockage composé de plusieurs disques durs.
Une carte audio aussi appelé (high definition audio) qui permet de gérer le son.
Le PCI qui est un bus grâce auquel les cartes graphiques communiquent.
L'USB 2  et l'USB 3 qui permettent de faire la jonction entre des périphériques
externes et la carte mère.
Le BIOS : qui permet au PC d'initialiser les connexions avec ses périphériques
à l'allumage et d'autres encore que j'oublie certainement ! (n'hésitez pas à me le faire remarquer!) .


On différencie les cartes mères grâce à ces 4 facteurs :
le facteur d'encombrement qui défini les dimensions de la carte mère.
Le chipset
Le support du processeur ou socket qui est le port destiné au processeur
ses fonctions ajoutées


3. Le processeur


			Le processeur est le cœur de l'ordinateur, c'est lui qui traite et
exécute les programmes stockés dans l'ordinateur. Une horloge lit les
instructions du programme à chaque tic donc plus la fréquence de l'horloge est
élevé et plus le processeur lira les instructions rapidement.
Mais le processeur ne s'arrête pas à une simple horloge, il faut aussi que son
rendement soit bon et qu'il ne soit pas trop énergivore. La mémoire cache du
processeur est tout aussi importante, elle permet de stocker les données
redondante demandées par le processeur et plus elle est grande, plus on peut
stocker de données et donc éviter au processeur des calculs inutiles.
Comme il est aujourd'hui impossible d'augmenter la fréquence de l'horloge, on a
ajouté des « coeurs » qui permet de gagner du temps lors d’exécution de
commandes en multitâches. Le socket (connexion à la carte mère) est tout aussi
important, plus il y a de trous dans le socket et plus il y aura d'échanges
entre le processeur et la carte mère. On appelle les sockets en fonction du
nombre de contacts qu'il possèdent (ex : socket 1564 → 1564 contacts).


4. La mémoire


			Nous allons maintenant nous intéresser à la mémoire. Les deux types
de mémoires sont la mémoire ROM (conservée par le PC hors alimentation) et la
mémoire RAM (perdue lors de la mise hors tention du PC).


5. Carte graphique


			La carte graphique est très utilisée pour faire tourner des jeux
vidéos et des logiciels de modélisation.La carte graphique  permet de convertir
des données brutes en données affichables sur un support (ecran …). Elle peut
aussi depuis quelques temps prendre en charge les lourds calculs nécessaires à
la 3D que le processeur devait auparavant faire. Elle contient un processeur
intégré (aussi appelé GPU). C'est lui qui va soulager le processeur principal
lors des calculs et qui va améliorer la qualité des images. Le GPU est souvent
associé à un radiateur et un ventilateur lorsqu'on l'achète qui sont nécessaires
pour diminuer la température à proximité du processeur.
Le RAMDAC (Random access memory digital analog converter) qui permet la
conversion numérique/analogique des données qui seront ensuite affichables par
un support. Si le RAMDAC n'est pas suffisamment cadencé en fréquence, le nombre
d'images par secondes sera alors bridé. (Même si ce composant ne bride jamais en
réalité!)
La mémoire vidéo qui permet de stocker les textures. Elle peut aller de 16
(suffisant pour tout PC à 512 Mo(utile pour les jeux en particulier).
Les entrées/sorties vidéos pour relier à l'écran par exemple.
Les ports vers la carte mère (pour plus de précisions cf. à la norme PCI Express)
Il y a aussi d'autres options possibles comme l'antialiasing (diminue l'effet
hachuré comme sur le pointeur de la souris ou la main vue sur l’écran et lisse
l'objet (moins pixelisé si on veut)). Mais en échange on applique un effet de
flou qui peut être pallié par un filtrage anisotrope qui rend lui les objets
plus nets par la suite.
Et encore bien d'autres mais le nombre augmente vite !
