---
title: "Installation VM Ubuntu sur W10"
tags: Tuto
author: "Axel CHEMIN"
co-author: "Titouan AZIMZADEH, Corto CALLERISA, Nicolas GUTIERREZ"
date: 2020-09-09 11:05:17
license: MIT
---

# Explication VM (Virtual Machine)

Avant d'installer une VM (machine virtuelle, en: *virtual machine*), il est sûrement important de comprendre comment ça marche.
Un ordinateur c'est très compliqué, avec pleins de systèmes interconnectés. Mais on va prendre l'approche suivante.

Il y a plusieurs niveaux d'abstraction, ce que l'on peut traduire par "niveau de compréhensibilité d'un humain", avec le binaire tout en bas (bas niveau) et le texte affiché sur l'écran tout en haut (haut niveau). Regardons du bas vers le haut :
* Des petits signaux électriques représentant des 1 et des 0 circulent
* Ces petits signaux se regroupent stratégiquements sur plusieurs composants (Processeur, Bios, Carte graphique/son/réseau, etc.) qui sont connectés grâce à la carte mère
* Le processeur sait faire communiquer les différents éléments
* L'OS (système d'exploitation, eng: *operating system*, comme Windows, Mac, Linux, ...) permet la communication entre la machine et l'utilisateur humain
* Mais il est nécessaire d'avoir une couche de logiciel pour avoir des fonctionnalités intéressantes

Ainsi, théoriquement, on ne peut pas utiliser plus d'un OS sur une machine simultanément (le processeur ne peut obéir qu'à un seul). Donc après téléchargement/installation des OS :
 * Soit vous décidez au lancement de votre machine quel OS utiliser (**dual boot**, fait l'objet d'un autre tuto *[Bientôt]*)
 * Sinon vous utilisez une VM
 * Bon il existe d'autres solutions : WSL, docker (pour les plus intéressés, voir [ici](!http://clubinfopolytechlille.gitlab.io/tutos/Installer-le-sous-syst%C3%A8me-Linux.html) et ici *[Bientôt]*)

Pour terminer, une VM c'est l'intégration dans la couche logicielle d'un OS !

On utilisera les mots d'hôte (eng: "*host*") pour l'OS qui est installé en natif (celui qui se lance au démarrage de la machine), et d'invité (eng: "*guest*") pour ceux lancés via VirtualBox.

# Installation d'une VM en bref
L'installation de la VM ne requiert pas forcément un "Step by step", si vous connaissez un petit peu les concepts, n'hésitez pas à essayer sans? Autrement, il n'y a aucun mal, et vous irez plus vite en suivant le "Step by step" ci-dessous !
Si vous êtes encore sur cette section, il n'y a que que 2 choses à savoir :
* Le logiciel proposé ici est Virtual Box, proposé sous licence GNU (alt: Libre et du coup gratuit) et vraiment complet !
* Il vous faut un OS (on utilise plus bas Ubuntu), et plus précisément son "image disque"

# Installation d'une VM : "*step by step*"
 1. Aller sur https://www.virtualbox.org/
 1. Aller sur [Downloads](!https://www.virtualbox.org/wiki/Downloads) (menu de gauche OU gros bouton en bas)
 1. On y trouve en dessous de "#VirtualBox 6.1.10 platform packages" les liens utiles, [Windows hosts](!https://download.virtualbox.org/virtualbox/6.1.10/VirtualBox-6.1.10-138449-Win.exe) est celui qui nous intéresse
 1. Cliquez sur Exécuter puis Suivant

	![Invite de sécurité](VM_Setup/21inst-securite.PNG)
	![Details](VM_Setup/22inst-detail.PNG)
	![Raccourcis](VM_Setup/23inst-raccourcis.PNG)
 1. Ouvrer VirtualBox

	![Accueil](VM_Setup/31setup.PNG)
 1. Maintenant il faut télécharger l'OS guest pour pouvoir l'utiliser. Nous vous proposons de télécharger Ubuntu parce que c'est une version de Linux qui est facile à prendre en main, avec une très grosse communauté. Il se trouve sur le site d'Ubuntu dans la catégorie [#Download](https://ubuntu.com/#download), et sous Ubuntu Dekstop vous trouverais la version LTS (Long Term Service, version avec support à long terme pour maintenir la stabilité). Vous devez avoir récupéré un fichier .iso

	![Site Ubuntu](VM_Setup/41DL.PNG)
 1. On va pouvoir créer notre première machine virtuelle ! Sur VirtualBox appuiez sur "Nouvelle machine" ou Ctrl + N

	![Accueil](VM_Setup/31setup.PNG)
	![Nouvelle machine](VM_Setup/32naming.PNG)
 1. Le nom est à votre discrétion. Pour le dossier de la machine **faites attention** à choisir un dossier qui ne requiert pas les droits admin, le chemin par défaut est sûrement une bonne idée, sauf si vous avez un SSD, n'hésitez pas à l'utiliser. Ici on utilise Type:Linux et Version:Ubuntu, faites attention à prendre la version 32bits ou 64bits selon votre système. Pour connaître cette information, Démarrer+X > Système > Spécifications de l'appareil > Type du système ; vous pouvez aussi regarder le nom du fichier iso que vous avez téléchargé, il comportera cette information.
 1. Pour la RAM ça dépend de votre utilisation, si vous utilisez exclusivement la VM durant les sessions de travail, vous pouvez pousser le curseur. Je recommande d'être un peu avant le orange.

	![RAM](VM_Setup/35Ram.PNG)
 1. Créer un disque dur virtuel est une bonne idée

	![Disque](VM_Setup/36Disk.PNG)
 1. Dynamiquement

	![Disque](VM_Setup/38Disk.PNG)
 1. Emplacement du fichier, par défaut c'est bien (dans le même répertoire que la VM). 10Gio risque d'être un peu léger, encore une fois cela dépend de votre utilisation, 20/30Gio et vous seriez plus à l'aise (surtout que cette espace ne sera réellement utilisez **seulement** si vous les utilisez dans le guest).

	![Disque](VM_Setup/39Disk.PNG)
 1. Avant de pouvoir lancer la machine, elle est vide, il faut montrer le fichier d'installation .iso, précédemment téléchargé. Allez dans Configuration ou Ctrl+S > Stockage > Contrôleur : IDE > Ajout d'un lecteur optique > Créer > ...

	![Ajout iso](VM_Setup/42AjoutIso.PNG)
	![Disque optique selecteur](VM_Setup/43OpticalDiskSelector.PNG)
	![Disque optique selecteur](VM_Setup/43Choisir.PNG)
 1. On peut avoir quelques problèmes si on augmente pas la mémoire vidéo dans Affichage

	![Vidéo mémoire](VM_Setup/81RamVideo.PNG)
 1. On y est presque, on oublie VirtualBox, il faut démarrer la machine : Démarrer (Démarrage normal)
 1. Petit écran de chargement à fond noir

	![Installation Linux](VM_Setup/52Setup.PNG)
 1. On choisit la langue : français pour les captures d'écran, mais personnellement je préfère anglais, le support est plus fourni pour un système en anglais (bien que très complet déjà), et si ça peut vous aider dans l'apprentissage de cette belle langue ;) .

	![Installation Linux](VM_Setup/71LangInstall.PNG)
 1. On prend le clavier qui nous conviens (n'hésitez pas à tester avec la zone de texte prévu à cette effet, notamment les accents, symboles etc.)

	![Installation Linux](VM_Setup/72Clavier.PNG)
 1. Ici à vous de voir si vous voulez l'installation normale ou minimale, les mise à jours sont conseillés, et les logiciels tiers pas nécéssaire dans la VM

	![Installation Linux](VM_Setup/73SuiteL.PNG)
 1. Pas de panique, on efface tout... du disque que l'on a créé précédemment (qui est encore vide) ;) .

	![Installation Linux](VM_Setup/74Disque.PNG)
	![Installation Linux](VM_Setup/75DisqueAlert.PNG)
 1. Si vous êtes "an international person", vous pouvez mettre New York ou Tokyo, m'enfin bon ^^

	![Installation Linux](VM_Setup/76Heure.png)
 1. À votre discrétion, mais n'hésitez pas à noter toutes ces informations quelques part. Alors, moi il m'est arrivé que lors de l'entrée du mot de passe, qu'il soit écrit en QWERTY, gardez ça en tête pour la première connexion...

 ![Installation Linux](VM_Setup/77Login.png)
 1. Bienvenue

  ![Installation Linux](VM_Setup/78Setup.png)
 1. Redémarrer

  ![Installation Linux](VM_Setup/79Reboot.png)
 1. Là c'est à vous de voir, si vous sentez des ralentissements, on peut augmenter la mémoire vidéo

  ![Installation Linux](VM_Setup/81RamVideo.PNG)
 1. Je vous conseille aussi fortement d'ajouter l'intéraction entre l'hôte et l'invité : ça permet d'avoir un presse papier commun par exemple.

  ![Installation Linux](VM_Setup/82Add.PNG)


# Conclusion

Voilà, vous êtes tout prêt pour utiliser votre nouveau système d'exploitation, bon courage si c'est une découverte !
