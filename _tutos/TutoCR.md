---
title: "Comment faire un compte rendu."
tags: Tuto
author: "Simon Blas"
date: 2018-03-28 12:03:17
---

Bonjour à toutes et à tous, voici un petit tutoriel sur comment faire des comptes rendu.
Je m'efforcerais donc de vous expliquer la procèdure à suivre (qui est très simple), et de vous donner quelques bases de markdown.

## Procèdure

Voici les étapes à suivre, dans l'ordre, afin d'avoir un compte rendu publié sur 
le magnifique site du club Info:
- Se connecter sur GitLab, et se rendre dans le répertoire du club Info
- Disposer des droits suffisants pour créer et modifier des fichiers
- Se rendre dans le répertoire "crs", puis cliquer sur le '+', puis New File dans le menu déroulant qui s'affichera
- Dans ce nouveau fichier, modifier le titre à annee-mois-jour-compte-rendu-reunion.md
- Au début du compte rendu, ajouter un en-tête sous le format:
    - title : "titre de mon compte rendu"
    - tags : CR
    - author : "prénom nom"
    - Avec trois - sur les lignes précedant et suivant l'entète

Et voila vous avez tout ce qu'il vous faut pour pouvoir rédiger un compte rendu tout propre :).

Il sera rédigé en markdown, dont vous pouvez trouver la syntaxe (très simple d'utilisation) 
sur internet, mais voici les choses simples dont vous aurez potentiellement toujours besoin:

# Utiliser un #  pour faire un gros titre
## Utiliser deux #  pour faire un titre plus petit
### Et ainsi de suite :)

N'oubliez l'espace après le #.

Pour mettre en gras, encadrer le texte ou bout de texte avec des **.

**Ceci est en gras**

Faire des listes est très simple, mettez un : puis appuyez sur entrée:
- Ensuite mettez un tiret, un espace, et vous avez un élèment de votre liste
- Et plus si nécessaire

Enfin, dernière chose, pour mettre un lien:
   [texte du lien](url_du_lien)
On peut même choisir le nom qu'on lui donne :).

Voila voila, c'est tout pour ce petit simple tutoriel.

Si vous avez des soucis, n'hésitez pas à me demander de l'aide le mercredi midi à la réu du club Info ^^.

Une bonne journée à tous.
