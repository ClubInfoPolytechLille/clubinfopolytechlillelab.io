---
title: "Introduction au chess engine"
tags: chess
author: "Hugo Aillerie"
date: 2021-05-01 18:33:17
---

Ecrire un Chess Engine
=================

Un chess engine ou moteur de jeu d'echecs est une sorte d'ia qui joue aux échecs.

Un peu d'histoire...
----------------------

Au 18ème, Le premier moteur de jeu d'échecs apparaît avec le turc mécanique, un automate avec la faculté de jouer aux échecs. Ah, j'ai oublié de vous dire en faite, c'est de la triche, un humain le controlais mais bon il a quand même réussi a jouer contre Napoléon, ou Catherine 2 de Russie donc la classe 

C'est en 1950 que les premiers chess engine apparaissent avec "Turbochamp" développé par Alan Turing lui-même!

Ensuite, ca évolue jusqu'en 1996, arrivé de Deep Blue développé par IBM. Deep Blue affronte le champion d'échecs de l'époque Garry Kasparov. Et Deep Blue perd mais prend sa revanche l'année d'après avec quelques amélioration et il gagne alors 3,5 à 2,5. Petite anecdote de type croustillante, Kasparov lors du match retour a perdu le deuxième match contre Deep Blue suite a une erreur de l'IA due a priori a un bug. Kasparov n'a alors pas profité de l'erreur, le coup lui parraissant bizarre, il l'a considéré comme une sorte de piège...

Depuis on a des chess engine qui s'améliore d'année en année avec notamment StockFish, Komodo... Mais fin 2017, révolution dans l'évolution, Deep Mind développe AlphaZero pour le go qu'il adapte ensuite pour le jeu d'échecs. AlphaZero gagnera contre StockFish (le meilleur a ce moment). AlphaZero jouait de manière plus intuitive et humaine qu'un chess engine "classique" comme StockFish (On y reviendra)

Comment va réflechir le ChessEngine ?
---------------------------------------------

Il existe grosso modo, deux sortes de chess engine, ceux "classiques" ou "traditionnels", et ceux "à la AlphaZero".

AlphaZero utilise l'apprentissage par renforcement lié à des réseaux de neuronnes. C'est incroyable mais ce n'est pas ce que je vais développer ici. vous retrouvez plus d'info [ici](https://deepmind.com/blog/article/alphazero-shedding-new-light-grand-games-chess-shogi-and-go)

Les moteurs de jeu traditionnels simulent plusieurs coups à l'avance. Puis ils sélectionnent celui qu'ils trouvent le mieux. On va dégrossir tout ca ensuite.

La représentation du plateau
---------------------------------

Avant toute chose, il va falloir choisir la facon de représenter le plateau!
C'est la base de votre chess engine.

Plusieurs facon de voir les choses : 
* Une matrice 8x8 ou tableau de 64 cases pour représenter les 64 cases du plateau. (simple efficace)
* Un tableau 10x12 pour s'affranchir des problèmes de dépassement du plateau par les pièces ([voir ici](https://www.chessprogramming.org/10x12_Board))
* Le représenter avec 12 bitboard (12x 64 bit), un bitboard représente une pièce d'une couleur (ex: les pions blancs) pour chaque 1 on a cet pièce sur cet case du plateau et 0 la pièce ne s'y trouve pas

Pourquoi choisir une représentation plutot qu'une autre, la matrice 8x8 et la plus intuive, le tableau 10x12, c'est la matrice 8x8 mais en futé, avec celle-ci, on peux chosir d'etre plutôt haut niveau, comme avec des classes pour chaque pièce, ou sinon plutôt bas niveau, en représentant les pièces avec 2 octets par exemple.
La représentation avec les 12 bitboard est a priori la plus bas niveau.
Plus on est bas niveau, meilleure sera la perfomance mais plus dur est de terminer un tel projet.

Après avoir choisis la représentation, il faut rajouter les différentes règles du jeu et les déplacements de chaque pièce.

Le cerveau du chess engine
--------------------------------

#### La fonction d'évaluation

Il sera important pour le chess engine de pouvoir évaluer une position.
Pour cela, on utilise **la fonction d'évaluation**.
Elle peut etre très simple : attribuer pour chaque pièce une valeur, exemple : pion 1, cavalier 3...
puis faire la somme des valeurs de nos pièces en soustrayant celle de l'adversaire.

Elle peut etre un peu plus complexe en prenant en compte la protection du roi, les menaces, les cases sur lesquelles se trouvent les pièces...

#### La recherche

L'idée du moteur de jeu est de simuler un nombre de coup important pour aller chercher  la meilleur position possible. Il choisira ensuite le coup qui lui permetra d'avoir la meilleure en considérant que l'adveraire joue les meilleures coups possibles. Il s'agit là de l'algorithme Alpha Beta ou Min Max (alpha beta est une opti de minmax).
Il existe bien entendu d'autres facons de faire comme la méthode de recherche aborescente de monte carlo, mais celle-ci s'applique moins bien aux moteurs de jeu "classiques".

-------
Un des défis sera donc d'avoir la meilleure fonction d'evaluation possible mais plus celle-ci est complexe plus elle prendra de temps a calculer est moins on explorera de coups, il faut donc trouver un bon compromis entre exploration et l'évaluation.

-------
Ensuite une table de transposition peut-être mis en place pour ne pas recalculer certaine position déjà calculé. C'est très intéressant pour gagner du temps de calcul.
De la même manière, une base d'ouverture et de finale peut-être mise en place.



Le protocole de communication
------------------------------------

Ok top, on peux avoir un chess engine avec tout ca mais dans l'idée on aimerais pouvoir le faire communiquer avec les autres chess engine pour les mesurer l'un à l'autre ou tout simplement jouer contre notre chess engine à l'aide d'un GUI. Ca permet aussi de connecter notre chess engine à un compte lichess par exemple.

Pour cela il existe deux protocoles de communication utilisé dans le milieux de la programmation des échecs, UCI et WinBoard. Je ne détaillerais qu'UCI car il est plus universelle et aussi par que je n'ai utilisé que celui-ci ;)

#### UCI

Tout se trouve [ici](http://wbec-ridderkerk.nl/html/UCIProtocol.html)
Mais je vais quand meme vous donner les points essentielles
voilà les réponses attendues après ces différentes commandes

**commande** :
* réponse du chess engine (detail) 

**uci** :
* id name xxx (on dis qui on est) 
* id author hugo
* registration ok
* uciok

**isready** :
* readyok (on dis que l'engine est prêt) 

**ucinewgame** : (debut d'une nouvelle partie, n'attend pas de réponse) 

**position startpos e2e4 e7e5 g1f3** : (indique la position, ici la position est la position de départ suivie des trois coup 1. e4 e5 2. Cf3, n'attend pas de réponse) 

**go** :
* bestmove b8c6 (donne le coup a jouer) 

Voilà ca s'est vraiment la base

-------
Maintenant a vous de jouer !

Sources utiles : 
* [uci](http://wbec-ridderkerk.nl/html/UCIProtocol.html)
* [la bible](https://www.chessprogramming.org/Main_Page)
* [un moteur de base est simple sans protocole uci](https://github.com/Tazeg/JePyChess), on l'a modifié avec le protocole uci si vous le souhaitez.
