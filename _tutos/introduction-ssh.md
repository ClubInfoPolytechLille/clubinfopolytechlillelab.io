---
title: Introduction au SSH
tags: ssh
author: "Geoffrey Preud'homme"
date: 2015-11-20 15:25:15
---
# Introduction au SSH

## Théorie

SSH est un protocole de communication permettant de se connecter d'une machine à une autre. 

Commande principale (pour un environnement Linux, déjà)
`ssh utilisateur@nom_de_la_machine -p port` (si `-p port` n'est pas spécifié, c'est 22 par défaut, si `utilisateur@` n'est pas spécifié, c'est votre nom de session actuel (`echo $USER`) qui est utilisé)


Pour transferer un fichier d'une machine à l'autre : `scp -P port source destination` (notez le `P` majuscule cette fois). On rajoutera l'option `-r` juste après `scp`pour transferer un dossier et son contenu.

Si `source` et/ou `destination`est une machine distante, on utilisera la notation `utilisateur@nom_de_la_machine:emplacement_du_fichier`.

## Pratique
### Depuis l'exterieur (IPv4)
Pour se connecter depuis l'exterieur au réseau Polytech Lille :

* **Serveur :** `portier.polytech-lille.fr`
* **Port :** `2222`
* **Nom d'utilisateur :** votre identifiant Polytech
* **Mot de passe :** votre mot de passe Polytech

 Vous arriverez sur la machine `weppes`. À partir de là, vous pouvez vous connecter à toutes les machines (par exemple `ssh florine06`) et faire comme si vous étiez à Polytech.

**Note :** Actuellement tout `scp`passant par `weppes` ne fonctionne pas (on obtient un message du type `C0664 50 nomfichier`.
**Note pour les GIS et IMA :** pour se connecter aux machines des salles Redon le nom de domaine est `zabethXX.plil.info` (XX est le numéro de la machine), et le port est `2222`.

### Depuis l'exterieur (IPv6)
Si vous avez la chance d'avoir une connectivité IPv6 chez vous (ce que vous pouvez vérifier avec <http://ipv6-test.com/>, vous pourrez vous connecter à N'IMPORTE QUELLE macine du réseau Polytech avec son adresse IPv6.
Pour connaître une telle adresse, connectez vous à cette machine (via la méthode IPv4 par exemple), et entrez `ip addr show eth0`.

Vous obtiendrez un truc du genre `2001:660:4401:6004:21a:a0ff:fe26:9672`(ici pour Astruc01), que vous pourrez utiliser avec `ssh`.

### Imprimer un fichier
Sur un ordinateur (et pas un serveur tel que `weppes` ou `houpplin`), entrez la commande suivante `lpr -H Imprimante fichier.pdf`. Les imprimantes possible sont visibles avec `lpstat -a`.
