---
title: "Brève introduction au SSH"
tags: ssh
author: "UnIma"
date: 2020-07-07 15:30:17
---

# Brève introduction au SSH
**Connexion aux serveurs SSH de Polytech, commandes
principales, et installation du client SSH sur Windows 10
et Android**


Sommaire
============
1. [Attributions](#attributions)
2. [Préambule](#pr-ambule)
3. [“SSH”, qu’est-ce que c’est ?](#ssh-qu-est-ce-que-c-est)
4. [Les commandes pour se connecter en SSH à Polytech](#les-commandes-pour-se-connecter-en-ssh-polytech)
5. [Copier des fichiers en SSH](#copier-des-fichiers-en-ssh)
6. [Installer le client SSH](#installer-le-client-ssh)
    1. [Sur Android](#sur-android)
    2. [Sur Windows 10](#sur-windows-10)


Attributions
============

Une partie de ce document réutilise les explications publiées sur
https://doc.ubuntu-fr.org/ssh écrit par Wiki ubuntu-fr La documentation
francophone sous le titre SSH couvert par la licence Creative Commons CC
BY-SA 3.0 <https://creativecommons.org/licenses/by-sa/3.0/deed.fr>

Document publié sous licence Creative Commons CC BY-SA 3.0
<https://creativecommons.org/licenses/by-sa/3.0/deed.fr>

Pour en savoir plus sur les outils de travail informatique, vous pouvez
vous rendre sur :
<https://gitlab.univ-lille.fr/julien.forget/travail-a-distance>.\
Ecrit par Forget & Co., vous retrouverez des astuces pour transférer et
partager des fichiers, utiliser Linux et les logiciels de Polytech
depuis chez vous. Des exemples de scp et de ssh sont directement repris
de ce dépôt git.

Préambule
=========

Ce tutoriel a pour objectif d’expliquer les bases du SSH pour se
connecter à distance sur sa session Polytech. Ce document n’a pas pour
but de se substituer à la documentation de SSH. Il s’agit plutôt d’un
memento qui rappelle les bases pour pouvoir se connecter aisément sur
les serveurs de Polytech. Vous trouverez les commandes de base pour se
connecter sur sa session, copier des fichiers, ainsi que les étapes pour
configurer un client SSH sur Windows et Android. Nous parlerons
seulement du mode de connexion par mot de passe, et du client SSH; la
connexion par clés publique/privé ainsi que le serveur SSH ne seront pas
abordés.

“SSH”, qu’est-ce que c’est ?
============================

SSH est un protocole permettant d’établir une communication chiffrée,
donc sécurisée (on parle parfois de tunnel), sur un réseau informatique
(intranet ou Internet) entre une machine locale (le client) et une
machine distante (le serveur). La sécurité du chiffrement peut être
assurée par différentes méthodes, entre autres par mot de passe ou par
un système de clés publique / privée (cryptographie asymétrique). SSH
remplace de manière sécurisée :

-   Telnet : en SSH, on peut exécuter des commandes depuis un réseau
    local ou Internet, comme on le faisait avec Telnet, mais de manière
    plus sécurisé (Telnet est en clair vs SSH est crypté).

-   FTP : là aussi SFTP apporte une sécurité supplémentaire par rapport
    à FTP qui est en clair.

-   Et d’autres, via le « tunneling » qui consiste à établir un tunnel
    entre un client et un serveur en utilisant le protocole SSH.

Les usages de SSH sont entre autres :

-   Accéder à distance à une console en ligne commande (shell), ce qui
    permet d’effectuer la totalité des opérations courantes et/ou
    d’administration sur la machine distante.

-   Déporter l’affichage graphique de la machine distante.

-   Transférer des fichiers en ligne de commande.

-   Monter ponctuellement des répertoires distants, soit en ligne de
    commande, soit via Nautilus sous GNOME par exemple.

-   Monter automatique des répertoires distants.

-   Déporter de nombreux autres services ou protocoles.

Dans notre tutoriel, nous nous intéresserons seulement au client SSH, le
serveur SSH sera le serveur de Polytech. Pour l’authentification, nous
nous intéresserons uniquement à la connexion par mot de passe (id
Polytech).

Les commandes pour se connecter en SSH à Polytech
=================================================

Supposons que l’on s’appelle John Doe et que notre identifiant Polytech
soit jdoe. L’identifiant par défaut sera l’identifiant de notre session
utilisateur. Le port par défaut est le 22.

La commande de base :

``` {.shell}
$ ssh -p <port> <id>@<serveur>
```

*Regarder la doc pour plus d’informations sur la commande ssh (man
ssh)*.

Si on n’est pas connecté sur le réseau Polytech :

``` {.shell}
$ ssh -p 2222 jdoe@portier.polytech-lille.fr
```

Si on est connecté sur le réseau Polytech :

``` {.shell}
$ ssh jdoe@arsenic.polytech-lille.org
```

ici on n’a pas spécifié de port avec l’argument -p, donc on se connecte
via le port par défaut, c’est-à-dire le port 22,

Une fois que l’on est connecté sur le réseau Polytech, on peut faire un
ssh sur une autre machine de Polytech (et utiliser ses ressources) :

``` {.shell}
$ ssh bimberlot14
```

\
On n’a pas spécifié de port avec l’argument -p, donc on se connecte via
le port par défaut, c’est-à-dire le port 22.\
On n’a pas spécifié de nom d’utilisateur, donc on utilise celui de la
session utilisateur, c’est-à-dire notre id Polytech, ici jdoe.\
On n’a pas non plus spécifié un nom de serveur, mais le nom de la
machine sur le réseau, car on est toujours sur les serveurs de Polytech.

En allant sur :\
<https://gitlab.univ-lille.fr/julien.forget/travail-a-distance/-/blob/master/outils.md#ssh-multi>\
On peut apprendre plusieurs astuces pour se connecter sur une machine de
Polytech et y rester connecté après l’extinction automatique des
machines de Polytech.

Pour connaître les machines de TP actuellement allumées on peut utiliser
le script :
<https://gitlab.univ-lille.fr/julien.forget/travail-a-distance/-/blob/master/list_machines.sh>\
Il faut ajouter les droits à ce fichier : |chmod 700 list\_machines.sh|
et puis l’exécuter avec |./list\_machines.sh|

Pour rester connecté après 18h30, tapez la commande |super halt\_| dont
l’effet porte sur 24h.

Pour lancer des programmes en mode graphique, on rajoute l’option -X :

``` {.shell}
$ ssh -X jdoe@arsenic.polytech-lille.org
```

Pour se déconnecter, on tape tout simplement la commande : |exit|

Copier des fichiers en SSH
==========================

Pour copier des fichiers de sa session Polytech vers sa machine
personnelle (MP), on utilise la commande scp. Attention cette commande
doit être lancée depuis la MP ! On ne fait pas de ssh sur le serveur
Polytech pour faire ensuite un scp.

La commande de base :

``` {.shell}
$ ssh -r -p <port> <id>@<serveur>:<chemin session Polytech>
<chemin cible sur la MP>
```

*Regarder la doc pour plus d’info sur la commande scp (man scp).*

\
-r signifie « récursif », c’est-à-dire que l’on va copier le dossier et
tous les fichiers et sous-répertoires.\
Pour la partie -p &lt;port&gt; &lt;id&gt;@&lt;serveur&gt;, on fait
exactement comme pour la commande ssh, suivant le réseau sur lequel on
est connecté (wifi Polytech ou pas).

On veut copier notre répertoire situé sur notre session Polytech à
l’adresse  /Documents/CodeInfo/ sur notre MP à l’adresse
 /Bureau/Backup/.

Si on n’est pas connecté sur le réseau Polytech :

``` {.shell}
$ scp -r -p 2222 jdoe@portier.polytech-lille.fr:~/Documents/CodeInfo/
~/Bureau/Backup/
```

Si on est connecté sur le réseau Polytech :

``` {.shell}
$ scp -r jdoe@arsenic.polytech-lille.org:~/Documents/CodeInfo/
~/Bureau/Backup/
```

Pour copier un seul fichier de sa MP vers sa session Polytech :

``` {.shell}
$ scp toto.txt jdoe@arsenic.polytech-lille.org:~/
```

Installer le client SSH
=======================

Comme dit précédemment, on s’intéresse seulement au client SSH.
L’ensemble des commandes que nous avons listées sont accessibles par
défaut sur les distributions de Linux les plus courantes. Si ce n’est
pas le cas, regarder la documentation en ligne et installer les paquets
correspondants.

Sur Android
-----------

Plusieurs applications sont disponibles sur le Google Playstore. Nous
allons présenter ici un tutoriel pour JuiceSSH.

Tout d’abord rendez-vous à l’adresse suivante sur le Google Playstore et
installez Juice SSH:
https://play.google.com/store/apps/details?id=com.sonelli.juicessh

Une fois installée, ouvrez l’application, puis allez sur « Connexions »,
et appuyez sur le « + » (plus). Renseignez les informations suivantes :\
\
Type : SSH\
Adresse : arsenic.polytech-lille.org\
Identité &gt; Nouveau &gt; Utilisateur : jdoe (laissez les autres champs
vides)\
Port : 22\
Laissez tout le reste par défaut !

Ensuite, rendez-vous une deuxième fois dans l’onglet « Connexions », et
appuyez sur le « + » (plus).\
\
Type : SSH\
Adresse : portier.polytech-lille.fr\
Identité : jdoe\
Port : 2222\
Laissez tout le reste par défaut !

Une fois que vous avez configuré vos modes de connexion SSH, il vous
suffit d’appuyer sur portier.polytech-lille.fr ou
arsenic.polytech-lille.org dans l’onglet connexion ou bien dans le menu
principal, rubrique « Connexions fréquentes ».

Sur Windows 10
--------------

Allez dans : Paramètres Windows &gt; Applications &gt; Gérer les
fonctionnalités facultatives. Ensuite, cliquez sur « OpenSSH Client »,
puis sur « installer ». Enfin, redémarrez votre ordinateur.

Vous pourrez alors utiliser la commande |ssh| sur le terminal CMD ou
Powershell.


--------------
**Informatique, Microélectronique, Automatique**  
**Spécialité Systèmes Communicants**  
**Polytech’Lille - Université de Lille**

*13-06-2020*  
*CC-BY-SA 3.0*
