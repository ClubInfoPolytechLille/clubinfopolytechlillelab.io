---
title: "Git"
tags: git
author: "Benoit 'badetitou' Verhaeghe"
date: 2017-04-01 17:17:17
license: WTFPL
license_url: http://www.wtfpl.net/
---

# Présentation

Git est un outil de gestion de version

Lors du développement d'une application, il arrive souvent que plusieurs personnes travail sur le même projet. Le plus grand des problèmes est donc _Comment vais-je faire travailler tout le monde sans pour autant tout casse ?_ Mais aussi, si quelqu'un casse tout, comment revenir en arrière. Evidemment, le mail n'est pas la solution. _Qui a fait quoi ? Quand ? Tu es à quelle version ? Mince toi aussi tu as modifié le fichier ?_

C'est pour cela que git existe. Il va nous permettre de développer un projet de manière **propre**

# Mais comment ça fonctionne ?

Vous avez un serveur git (local, ou en ligne). Vous avez un client git (généralement sur votre machine). Vous communiquez depuis votre client vers le serveur.

_Comment ?_

Lorsque vous créez des fichiers pour votre projet, vous dites à votre client de les ajouter au projet. Ensuite, quand vous modifiez un fichier, vous **commentez** vos modifications. Enfin, vous ajoutez les modifications sur le serveur.

_Et la collaboration ?_

Les personnes que vous avez autorisées peuvent télécharger le projet sur leur ordinateur avec un client git, faire des modifications, et envoyer des propositions de correction.

_Mais si on modifie la même chose ?_

On peut voir git comme un système de calque. Au début, on a un calque. Ensuite chacun copie la calque pour lui-même. 
On modifie tous notre calque et ensuite on combine l'ensemble des calques pour mettre à jour le premier. 
Dès qu'une modification est validée (par nous), on combine notre calque et le calque original. 
Un participant du projet peut copié le calque original à n'importe quel moment (après ou avant des modifications). 
Si on veut combiner un calque que nous avons créé avec le calque original (qui a été modifié depuis). 
Et que des modifications se confronte, on doit créer un calque de correction qui permet de contenter les modifications de tout le monde.

_Euh... Pourquoi git ? Ca fait beaucoup à apprendre_

C'est vrai que git n'est pas facile à prendre en main. 
Mais vim non plus, et pourtant il n'y a rien de mieux. 
Plus sérieusement, apprendre git c'est un investissement au départ mais qui vous servira toute votre vie. 
Travail en équipe, gestion de version, mise en production par version (docker), résolution de bug,... De plus, les entreprises utilisent git.

# Installation

Installation de Git chez vous

## Linux

`apt install git`

## Windows

[msysgit executable](http://msysgit.github.io)

## Mac

[git osx installer](http://sourceforge.net/projects/git-osx-installer/)

## Polytech Lille

Git peut fonctionner grâce au protocole SSH et HTTPS. SSH ne peut pas être utilisé à Polytech (question de sécurité réseau). C'est pour cela que nous verrons l'utilisation avec HTTPS ici.

L'utilisation de SSH reste similaire.

Afin d'utiliser le proxy HTTPS de Polytech avec GIT vous devez exécuter la commande suivante dans un terminal. 
Vous pouvez évidemment simplifier la manipulation en ajoutant la ligne suivante dans votre ~/.bashrc.

```bash
export http_proxy=proxy.polytech-lille.fr:3128
export https_proxy=$http_proxy
export ftp_proxy=$http_proxy
export rsync_proxy=$http_proxy
export no_proxy="localhost,127.0.0.1,localaddress,.localdomain.com"
```

# Commandes de base

## git init

Permet de créer un repository sur la machine local. Contient l'ensemble du projet. C'est ce que l'on retrouve sur chaque machine possédant le projet.

## git clone [url]

Permet de récuperer un repository distant.

## git pull

Permet de mettre à jour un repository.

## git add [nom de fichier/dossier]

Permet d'ajouter un fichier ou projet au repository.

## git commit -m "[commentaire]"

Permet de commenter les modification effectuer et les prépares à la validation.

## git push

Permet de valider les modifications effectuées.
