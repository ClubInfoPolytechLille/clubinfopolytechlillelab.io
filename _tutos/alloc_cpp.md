---
title: "(Ré-)allocations en C++"
tags: cpp alloc
author: "Jean Loup Beaussart"
date: 2016-03-19 14:56:00
---

Bonjour à tous les enfants, j'ai réfléchi pour l'histoire du realloc() en C++. Donc la réponse est non, il n'y a pas d'équivalent pour realloc() en C++.
Ensuite je me suis demandé : "Mais pourquoi Marianne utilise realloc()?" . Et là je me suis dit qu'elle faisait surement un truc du genre:

```c++
int *tab = (int*)malloc(10 * sizeof(int));

/* Elle remplit ses valeurs */

/* Un peu plus tard elle doit ajouter 5 nouvelles valeurs */

tab = (int*)realloc(tab, 15 * sizeof(int));
```


Et bien bonne nouvelle les enfants, en C++ plus besoin d'utiliser des tableaux classiques ( `int tab[10];` ).
La bibliothèque standard contient des objets que l'on appelle des conteneurs. En particulier il y a la classe `std::vector< int >` qui est un tableau dynamique.
Vous pouvez l'utiliser comme un tableau normal mais en plus il gère automatiquement son espace d'allocation.

Pour reprendre l'exemple ci-dessus:

```c++
std::vector< int> tab;

/* On met des valeurs dedans */
tab.push_back(42);
tab.push_back(666);

/* On peut les récupérer comme un tableau classique */
std::cout << tab[1];     //Ca va afficher 666

/* On peut encore rajouter des valeurs */
tab.push_back(45645616);

/* On peut compter le nombre d'éléments dans le tableau ou bien vider le tableau */
std::cout << tab.size();
tab.clean();
```

Et encore plein d'autre chose incroyable!!!!! 

